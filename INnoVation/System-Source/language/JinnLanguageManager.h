/***************************************************************************************************
 **  Copyright © 2017年 Jinn Chang. All rights reserved.
 **  Giuhub: https://github.com/jinnchang
 **
 **  FileName: JinnLanguageManager.h
 **  Description: 多语言管理
 **
 **  Author:  jinnchang
 **  Date:    2017/3/21
 **  Version: 1.0.0
 **  Remark:  Create New File
 **************************************************************************************************/

#import <Foundation/Foundation.h>

#define JinnLocalizedString(key, comment) [[JinnLanguageManager sharedInstance] internationalizedStringForKey:(key) value:@""]

extern NSString * const kAppLanguageDidChange; // APP语言被更改的通知

@interface JinnLanguageManager : NSObject

/**
 支持的语言列表，从 Languages.plist 中获取，每个语言都关键字和名称，如：简体中文，zh-Hans
 */
@property (nonatomic, strong, readonly) NSArray *languages;

+ (instancetype)sharedInstance;

/**
 当前语言，如果手动设置过以手动设置的语言为准，如果没有手动设置过以当前自动识别的语言为准

 @return 当前语言
 */
- (NSString *)currentLanguage;

/**
 对应关键字的国际化文本内容

 @param key 关键字
 @param value 注释，无实际意义
 @return 对应关键字的国际化文本内容
 */
- (NSString *)internationalizedStringForKey:(NSString *)key value:(NSString *)value;

/**
 手动切换语言

 @param index 对应语言的序号
 */
- (void)changeAppLanguageWithIndex:(NSInteger)index;

@end
