//
//  LanguageHelper.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/5.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import Foundation



func languageString(_ key: String, value: String = "")  -> String {
    
   return JinnLanguageManager.sharedInstance().internationalizedString(forKey: key, value: value)
}
