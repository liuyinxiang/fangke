/***************************************************************************************************
 **  Copyright © 2017年 Jinn Chang. All rights reserved.
 **  Giuhub: https://github.com/jinnchang
 **
 **  FileName: JinnLanguageManager.m
 **  Description: 多语言管理
 **
 **  Author:  jinnchang
 **  Date:    2017/3/21
 **  Version: 1.0.0
 **  Remark:  Create New File
 **************************************************************************************************/

#import "JinnLanguageManager.h"

static NSString * const kAppLanguage = @"APP_LANGUAGE";

NSString * const kAppLanguageDidChange = @"kAppLanguageDidChange";

@implementation JinnLanguageManager

@synthesize languages = _languages;

#pragma mark - Public

+ (instancetype)sharedInstance
{
    static dispatch_once_t once = 0;
    static JinnLanguageManager *languageManager = nil;
    
    dispatch_once(&once, ^{
        languageManager = [[self alloc] init];
    });
    
    return languageManager;
}

/**
 当前语言，如果手动设置过以手动设置的语言为准，如果没有手动设置过以当前自动识别的语言为准
 
 @return 当前语言
 */
- (NSString *)currentLanguage
{
    NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"][0]; // [[[NSBundle mainBundle] preferredLocalizations] firstObject]
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    // 以下部分为了做iOS版本兼容
    
    if ([language containsString:@"zh-Hans"])
    {
        language = @"zh-Hans";
    }
    else if ([language containsString:@"en"])
    {
        language = @"en";
    }
    else if ([language containsString:@"zh-Hant"])
    {
        language = @"zh-Hant";
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    NSString *configLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:kAppLanguage];
    
    if (configLanguage) // 如果手动设置过以手动设置的语言为准
    {
        language = configLanguage;
    }
    
    return language;
}

/**
 对应关键字的国际化文本内容
 
 @param key 关键字
 @param value 注释，无实际意义
 @return 对应关键字的国际化文本内容
 */
- (NSString *)internationalizedStringForKey:(NSString *)key value:(NSString *)value
{
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:[self currentLanguage] ofType:@"lproj"];
    
    if (!bundlePath)
    {
        bundlePath = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    }
    
    return [[NSBundle bundleWithPath:bundlePath] localizedStringForKey:key value:@"" table:nil];
}

/**
 手动切换语言
 
 @param index 对应语言的序号
 */
- (void)changeAppLanguageWithIndex:(NSInteger)index
{
    NSAssert(index < self.languages.count, @"Please check Languages.plist！");
    
    NSString *language = [[NSUserDefaults standardUserDefaults] objectForKey:kAppLanguage];
    
    if (![language isEqualToString:self.languages[index][1]])
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.languages[index][1] forKey:kAppLanguage];
        
        if ([[NSUserDefaults standardUserDefaults] synchronize])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:kAppLanguageDidChange object:nil];
        }
    }
}

#pragma mark - Getter / Setter

- (NSArray *)languages
{
    if (!_languages)
    {
        _languages = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Languages" ofType:@"plist"]];
    }
    
    return _languages;
}

@end
