//
//  UIView+SD.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/27.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func sd_addsubViews(subviews:[UIView]) -> () {
        for item in subviews {
            addSubview(item)
        }
    }
    
}
