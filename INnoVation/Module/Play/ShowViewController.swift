//
//  ShowViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/28.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit


class ShowViewController: FullTitleVisualEffectViewController {

    
    let showViewW  : CGFloat    = screenWidth / 2 + 20
    let viewTag    : Int        = 1000
    var midIndex   : Int        = 3                  // 仅用于透明度变化的标识
    var changeMark : Bool       = false              // 用于动画反转的标识
    var viewArray  : [ShowView] = []
    let titleArray : [String]   = [ "0", "1", "2", "3", "4", "5", "6"]
    let imageView  = UIImageView()
    
    var imagechangeMark : Bool       = false //临时切换用
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        ceratUI()
        
        addTap()
        
    }
    
    func addTap() {
        
        // 左右滑动
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ShowViewController.makeSwipeAction(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(ShowViewController.makeSwipeAction(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        
        contentView?.addGestureRecognizer(leftSwipe)
        contentView?.addGestureRecognizer(rightSwipe)
        
        //点击
        
//        let clickTap = UITapGestureRecognizer(target: self, action: #selector(ShowViewController.clikeTap(_:)))
//        contentView?.addGestureRecognizer(clickTap)
    }
    
    func ceratUI() {
 
//        let gakkiImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!))
//        gakkiImageView.contentMode = .scaleAspectFill
//        gakkiImageView.image = UIImage.init(named: "Gakki")
//        contentView?.addSubview(gakkiImageView)
        
        contentView?.backgroundColor = .black
        
        //创建下方图片
        contentView?.addSubview(imageView)
        imageView.image = UIImage.init(named: "Bird")
        imageView.contentMode = .scaleAspectFill
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(screenWidth)
            make.height.equalTo(screenHeight / 2)
            make.bottom.equalTo(0)
        }
        
        //创建两个黑色遮罩
        let black1 = UIView()
        let black2 = UIView()
        black1.backgroundColor = .black
        black2.backgroundColor = .black
        black1.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        black2.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        contentView?.sd_addsubViews(subviews: [black1, black2])
        black1.snp.makeConstraints { (make) in
            make.centerX.equalTo(0)
            make.height.width.equalTo(280)
            make.centerY.equalTo(screenHeight / 2 - 30)
        }
        
        black2.snp.makeConstraints { (make) in
            make.centerX.equalTo(screenWidth)
            make.height.width.equalTo(280)
            make.centerY.equalTo(screenHeight / 2 - 30)
        }
        
        //创建蛇皮列表
        for i in 0..<titleArray.count {
            
            let oneShowView = ShowView()
            oneShowView.tag = i + viewTag
             uLog("oneShowView.tag == \(oneShowView.tag)")
            oneShowView.theIndex = self.titleArray[i]
            oneShowView.imageView.image = UIImage.init(named: "muji\(i+1)")
            
            if i == midIndex {
                oneShowView.alpha = 1
            }else{
                oneShowView.alpha = 0.4
            }
            
            let x  = -showViewW - 20  + (CGFloat(i) * showViewW / 2)
            let y1 = screenHeight / 2 - showViewW / 2 - 20
            let y2 = screenHeight / 2 - showViewW - 20
            
            if i % 2 == 0{ // 偶
                oneShowView.topType = true
                oneShowView.frame = CGRect(x: x, y: y1, width: showViewW, height: showViewW)
            }else{         // 奇
                oneShowView.topType = false
                oneShowView.frame = CGRect(x: x, y: y2, width: showViewW, height: showViewW)
            }
            
            contentView?.addSubview(oneShowView)
            viewArray.append(oneShowView)
        }
    }

}
