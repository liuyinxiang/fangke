//
//  JSViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/3.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import WebKit
import PromiseKit

class JSViewController: FullTitleVisualEffectViewController, WKNavigationDelegate, UIScrollViewDelegate, WKScriptMessageHandler{

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        titleView?.isHidden = true
        
        let webView = WKWebView()
        webView.scrollView.bounces = false
        webView.backgroundColor = .white
        webView.frame = (contentView?.bounds)!
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        webView.configuration.userContentController.add(WeakScriptMessageDelegate.init(self), name: "INnoVation")
        webView.configuration.userContentController.add(WeakScriptMessageDelegate.init(self), name: "scanQR")
        contentView?.addSubview(webView)
        
//        let path = Bundle.main.bundlePath
//        let url = URL.init(fileURLWithPath: path)
//        let htmlPath = Bundle.main.path(forResource:"AirPrint", ofType:"html")
//        let htmlContent = try!String.init(contentsOfFile: htmlPath!)
        //webView.loadHTMLString(htmlContent, baseURL: url)
        //interviewee
        //http://47.52.40.142/#/login/guard
//        let url = NSURL(string: "http://182.61.177.56/visitor/web/#/login/guard")
        let url = NSURL(string: "http://182.61.177.56/#/login/guard")

        //创建请求 http://18.191.208.172/visitor/web/admin.php
        let request = NSURLRequest(url: url! as URL)
        //加载请求
        webView.load(request as URLRequest)
        
//        //todo
//        let vc          = InfoViewController()
//        let model = VisitModel()
//
//        model.address   = "someWhere"
//        model.name1     = "name1"
//        model.iconType  = "3"
//        model.name2     = "name2"
//        model.time      = "time"
//        model.QRCode    = "strArr[5]"
//        model.visterId  = "312341"
//
//        //新增
//        model.language  = "1"  //语言 "0"中 "1"英 "2"越南
//        model.titleType = "0"  //title图片类型 "0"TMI "1"ELITE
//        model.QRType    = "1"  //二维码图片类型 "0"V "1"G
//
//        vc.model        = model
//        vc.backBlock    = { (printImage) -> Void in
//            self.sendPrint(printImage)
//        }
//        presentBottom(vc.self)

    }
 

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        Swift.print(message)//WKScriptMessage对象
        Swift.print(message.name) //name : nativeMethod
        Swift.print(message.body) //js回传参数
        
        
        if message.name == "INnoVation" {
            uLog("INnoVation 调用")
            let dic     = message.body as? NSDictionary
            let bodyStr = dic?["body"]as? String ?? ""
            
            print("bodyStr == \(bodyStr)")
            let strArr = bodyStr.components(separatedBy: "$")
            
            let model = VisitModel()
            
            uLog("strArr.count == \(strArr.count)")
            
            if strArr.count > 9 {
                
                model.address   = strArr[0]
                model.name1     = strArr[1]
                model.iconType  = strArr[2]
                model.name2     = strArr[3]
                model.time      = strArr[4]
                model.QRCode    = strArr[5]
                model.visterId  = strArr[6]
                
                //新增
                model.language  = strArr[7]  //语言 "0"中 "1"英 "2"越南
                model.titleType = strArr[8]  //title图片类型 "0"TMI "1"ELITE
                model.QRType    = strArr[9]  //二维码图片类型 "0"V "1"G
              
            }
            
            let vc          = InfoViewController()
            vc.model        = model
            vc.backBlock    = { (printImage) -> Void in
                self.sendPrint(printImage)
            }
            presentBottom(vc.self)
        }else if message.name == "scanQR" {
            uLog("scanQR 调用")
            scanQR()
        }else{
            
        }
        
    }
    
    func scanQR() {
       uLog("scanQR 调用")
        
        let vc = QRScanViewController()
        vc.backBlock = {(str: String) -> Void in
            uLog("闭包拿到了str = \(str)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                // your code here
                self.sendTime(str)
                
                let vc = ShowWebViewController()
                vc.linkUrl = str
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
       self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func sendTime(_ urlSting: String){
        
        let strArr = urlSting.components(separatedBy: "&")
        
        if let visit = strArr.last, visit.count > 9{
            
            let visitId = visit.suffix(visit.count - 9)
            print("visitId == \(visitId)")
            
            //urlSting
            firstly {
                CallApi(KJoeApi.scanTime(visit_id: String(visitId)))

            }.done { (result) -> Void in
                
            }.catch {  (error) -> Void in
                
            }
        }else{
            print("url有误")
        }
        
    }
    
    func sendPrint(_ image: UIImage) {
       
        
        let srcImage = image
        
        //翻转图片的方向
        let flipImageOrientation = (srcImage.imageOrientation.rawValue + 3) % 8
        //翻转图片
        let flipImage =  UIImage(cgImage:srcImage.cgImage!,
                                 scale:srcImage.scale,
                                 orientation:UIImage.Orientation(rawValue: flipImageOrientation)!
        )
       
 
        let printInfo = UIPrintInfo(dictionary:nil)
        printInfo.outputType = UIPrintInfo.OutputType.general
        printInfo.jobName = "My Print Job"
        
        // Set up print controller
        let printController = UIPrintInteractionController.shared
        printController.printInfo = printInfo
        
        // Assign a UIImage version of my UIView as a printing iten
        //let imageViewIcon1 = UIImageView(frame: CGRect(x:0, y:0, width:150, height:150))
        let printImage = flipImage
        printController.printingItem = printImage
        
        // Do it
        printController.present(from: self.view.frame, in: self.view, animated: true, completionHandler: nil)
        
    }
 
   
}





