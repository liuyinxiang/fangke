//
//  VisitModel.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/25.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class VisitModel: NSObject {

    var address      : String = ""  //访问工厂
    var name1        : String = ""  //访客人
    var name2        : String = ""  //访问姓名
    var iconType     : String = ""  //访问类型
    var time         : String = ""  //访问时间
    var QRCode       : String = ""  //二维码信息
    var visterId     : String = ""  //访客ID
    
    var language     : String = ""  //语言 "0"中 "1"英 "2"柬埔寨
    var titleType    : String = ""  //title图片类型 "0"TMI "1"ELITE
    var QRType       : String = ""  //二维码图片类型 "0" V "1"G
    
}
