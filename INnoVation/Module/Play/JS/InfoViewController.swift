//
//  InfoViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/4.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class InfoViewController: PresentBottomVC {
    
    var backBlock: ((_ printContent: UIImage) -> ())?
    
    var model : VisitModel?
 
    //工厂地址
    let addressLabel        = UILabel()
    let addressValueLabel   = UILabel() /*Maybe添加*/
    //访客人
    let name1Label          = UILabel()
    let name1ValueLabel     = UILabel()/*Maybe添加*/
    //访客类型
    let typeLabel           = UILabel()
    let typeValueLabel      = UILabel() /*Maybe添加*/
    //工厂icon
    let typeImageView       = UIImageView()
    let typeValueImageView  = UIImageView()/*Maybe添加*/
    //访问姓名
    let name2Label          = UILabel()
    let name2ValueLabel     = UILabel()/*Maybe添加*/
    //访问时间
    let timeLabel           = UILabel()
    let timeValueLabel      = UILabel()/*Maybe添加*/
    //id
    let idLabel             = UILabel()
    let idValueLabel        = UILabel()/*Maybe添加*/
    /*新增company*/
    let companyLabel        = UILabel()
    let companyValueLabel   = UILabel()
    //访问日期
    let dateLabel           = UILabel()
    let dateValueLabel      = UILabel()/*Maybe添加*/
    //底部黑线
    let bottomLine          = UIView()
    //底部标志
    let bottomSignLabel     = UILabel()
    

    
    //真正打印的页面
    let printView     = UIView()
    
    @objc func changeTheContent(){
        
        if self.backBlock != nil {
            
            //self.backBlock!()
            presentBottomShouldHide()
        }
    }
    
    override var joeControllerHeight: CGFloat {
        return 400
    }
    
    override var joeControllerWidth: CGFloat {
        return 500  
    }
    
    override var joePresentType: Int {
        return 1
    }
    
    override var joeTapDismiss: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor     = UIColor(red:0.18, green:0.58, blue:0.97, alpha:1.00)
        view.layer.cornerRadius  = 6
        view.layer.masksToBounds = true
        
        setupUI()
        
        JinnLanguageManager.sharedInstance().changeAppLanguage(with: 1)
        //print(languageString("来访信息", value: ""))
        
    }
    
    @objc func closeButtonClicked() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dissmissVC() {
        presentBottomShouldHide()
    }
   
    
    @objc func printTheInfo() {
        
        if let backBlock = backBlock {
            backBlock(printView.screenShot()!)
            presentBottomShouldHide()
        }
        
    }
        
    func setupUI() {
        
        let cannelBtn = UIButton()
        cannelBtn.setTitle("Cannel", for: .normal)
        cannelBtn.addTarget(self, action: #selector(dissmissVC), for: .touchUpInside)
        view.addSubview(cannelBtn)
        cannelBtn.snp.makeConstraints { (make) in
            make.left.top.equalTo(0)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
        
        let printBtn = UIButton()
        printBtn.setTitle("Print", for: .normal)
        printBtn.addTarget(self, action: #selector(printTheInfo), for: .touchUpInside)
        view.addSubview(printBtn)
        printBtn.snp.makeConstraints { (make) in
            make.right.top.equalTo(0)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
        
        printView.backgroundColor = .white
        view.addSubview(printView)
        printView.snp.makeConstraints { (make) in
            make.top.equalTo(40)
            make.width.equalTo(joeControllerWidth)
            make.height.equalTo(joeControllerHeight + 20)
        }
        
        guard let model = model else { return }
        
        
        let titleImageView = UIImageView()
        printView.addSubview(titleImageView)
        titleImageView.snp.makeConstraints { (make) in
            make.width.equalTo(100)
            make.height.equalTo(40)
//            make.left.equalTo(15)
            make.centerX.equalTo(printView)
            make.top.equalTo(20)
        }
        
        if model.titleType == "0"{
            
            titleImageView.image = UIImage.init(named: "tmi")
//            titleImageView.image = UIImage.init(named: "elite")
            
        }else if model.titleType == "1"{
         
            titleImageView.image = UIImage.init(named: "elite")
        }
        
//        let title = UILabel()
//        title.font = UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.bold)
//        title.text = languageString("来访信息", value: "")
//        printView.addSubview(title)
//        title.snp.makeConstraints { (make) in
//            make.left.equalTo(15)
//            make.top.equalTo(20)
//        }
        
        let whiteView = UIView()
        whiteView.layer.shadowColor = UIColor.black.cgColor
        whiteView.layer.shadowOffset = CGSize(width: 1, height: 1)
        whiteView.layer.shadowOpacity = 0.3
        whiteView.layer.shadowRadius = 2
        printView.addSubview(whiteView)
        whiteView.snp.makeConstraints { (make) in
            make.width.height.equalTo(160)
            make.top.equalTo(10)
            make.right.equalTo(-10)
        }
        
      
        
//        let imageViewQR = UIImageView()
//        imageViewQR.image = createQRForString(qrString: model.QRCode,
//                                              qrImageName: "")
//        whiteView.addSubview(imageViewQR)
//        imageViewQR.snp.makeConstraints { (make) in
//            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15))
//        }
        
//        //二维码里的logo
//        let QRLogoImageView = UIImageView()
//        QRLogoImageView.layer.cornerRadius  =  4
//        QRLogoImageView.layer.masksToBounds = true
//        imageViewQR.addSubview(QRLogoImageView)
//        QRLogoImageView.snp.makeConstraints { (make) in
//            make.width.height.equalTo(35)
//            make.center.equalToSuperview()
//        }
//        if model.QRType == "0"{
//            QRLogoImageView.image = UIImage.init(named: "V")
//        }else if model.QRType == "1"{
//            QRLogoImageView.image = UIImage.init(named: "G")
//        }
        
        
        
//        let labelArray = [addressLabel, name2Label, name1Label, timeLabel]
//        printView.sd_addsubViews(subviews: labelArray)
//
        printView.addSubview(companyLabel)
        companyLabel.textAlignment = NSTextAlignment.right
        labelConfig(companyLabel, str: "Company")
        companyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(80)
            make.right.equalTo(printView.snp_centerX).offset(-80)
        }
        
        printView.addSubview(companyValueLabel)
        companyValueLabel.textAlignment = NSTextAlignment.center
        companyValueLabel.layer.borderWidth = 1
        companyValueLabel.layer.borderColor = UIColor.black.cgColor
        companyValueLabel.textColor = UIColor.gray
        labelConfig(companyValueLabel, str: "Company")
        companyValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(companyLabel)
            make.left.equalTo(companyLabel.snp_right).offset(20)
            make.width.equalTo(260)
            make.height.equalTo(25)
        }
        
        printView.addSubview(name1Label)
        name1Label.textAlignment = NSTextAlignment.right
        labelConfig(name1Label, str: "Name")
        name1Label.snp.makeConstraints { (make) in
            make.top.equalTo(companyLabel.snp_bottom).offset(10)
            make.right.equalTo(companyLabel)
        }
        printView.addSubview(name1ValueLabel)
        name1ValueLabel.textAlignment = NSTextAlignment.center
        name1ValueLabel.layer.borderWidth = 1
        name1ValueLabel.layer.borderColor = UIColor.black.cgColor
        name1ValueLabel.textColor = UIColor.gray
        labelConfig(name1ValueLabel, str: model.name1)
        name1ValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(name1Label)
            make.left.equalTo(companyValueLabel)
            make.width.equalTo(companyValueLabel)
            make.height.equalTo(companyValueLabel)
        }
        
        printView.addSubview(name2Label)
        name2Label.textAlignment = NSTextAlignment.right
        labelConfig(name2Label, str: "Visiting")
        name2Label.snp.makeConstraints { (make) in
            make.top.equalTo(name1Label.snp_bottom).offset(10)
            make.right.equalTo(companyLabel)
        }
        printView.addSubview(name2ValueLabel)
        name2ValueLabel.textAlignment = NSTextAlignment.center
        name2ValueLabel.layer.borderWidth = 1
        name2ValueLabel.layer.borderColor = UIColor.black.cgColor
        name2ValueLabel.textColor = UIColor.gray

        labelConfig(name2ValueLabel, str: model.name2)
        name2ValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(name2Label)
            make.left.equalTo(companyValueLabel)
            make.width.equalTo(companyValueLabel)
            make.height.equalTo(companyValueLabel)
        }
        
        printView.addSubview(addressLabel)
        addressLabel.textAlignment = NSTextAlignment.right
        labelConfig(addressLabel, str: "Visit Factory")
        addressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(name2Label.snp_bottom).offset(10)
            make.right.equalTo(companyLabel)
        }
        printView.addSubview(addressValueLabel)
        addressValueLabel.textAlignment = NSTextAlignment.center
        addressValueLabel.layer.borderWidth = 1
        addressValueLabel.layer.borderColor = UIColor.black.cgColor
        addressValueLabel.textColor = UIColor.gray

        labelConfig(addressValueLabel, str: model.address)
        addressValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(addressLabel)
            make.left.equalTo(companyValueLabel)
            make.width.equalTo(companyValueLabel)
            make.height.equalTo(companyValueLabel)
        }
        
        
        let time = currentTime()
        let date = currentDate()
        
        printView.addSubview(dateLabel)
        dateLabel.textAlignment = NSTextAlignment.right
        labelConfig(dateLabel, str: "Date")
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(addressLabel.snp_bottom).offset(10)
            make.right.equalTo(companyLabel)
        }
        printView.addSubview(dateValueLabel)
        dateValueLabel.textAlignment = NSTextAlignment.center
        dateValueLabel.layer.borderWidth = 1
        dateValueLabel.layer.borderColor = UIColor.black.cgColor
        dateValueLabel.textColor = UIColor.gray

        labelConfig(dateValueLabel, str: date)
        dateValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateLabel)
            make.left.equalTo(companyValueLabel)
            make.width.equalTo(companyValueLabel)
            make.height.equalTo(companyValueLabel)
        }
        
        printView.addSubview(timeLabel)
        timeLabel.textAlignment = NSTextAlignment.right
        labelConfig(timeLabel, str: "Time In")
        timeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel.snp_bottom).offset(10)
            make.right.equalTo(companyLabel)
        }
        printView.addSubview(timeValueLabel)
        timeValueLabel.textAlignment = NSTextAlignment.center
        timeValueLabel.layer.borderWidth = 1
        timeValueLabel.layer.borderColor = UIColor.black.cgColor
        timeValueLabel.textColor = UIColor.gray

        labelConfig(timeValueLabel, str: time)
        timeValueLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(timeLabel)
            make.left.equalTo(companyValueLabel)
            make.width.equalTo(companyValueLabel)
            make.height.equalTo(companyValueLabel)
        }
        
        printView.addSubview(bottomLine)
        bottomLine.backgroundColor = UIColor.black;
        bottomLine.snp.makeConstraints { (make) in
            make.top.equalTo(timeLabel.snp_bottom).offset(10)
            make.height.equalTo(2)
            make.left.equalTo(printView).offset(40)
            make.right.equalTo(printView).offset(-40)
        }
        
        printView.addSubview(bottomSignLabel)
        bottomSignLabel.text = "VISITOR"
        bottomSignLabel.font = UIFont.systemFont(ofSize: 38, weight: .bold)
        bottomSignLabel.snp.makeConstraints { (make) in
            make.top.equalTo(bottomLine.snp_bottom).offset(5)
            make.left.equalTo(bottomLine.snp_left).offset(20)
//            make.right.equalTo(bottomLine.snp_right).offset(-20)
        }
        let imageViewQR = UIImageView()
        imageViewQR.image = createQRForString(qrString: model.QRCode,
                                              qrImageName: "")
        whiteView.addSubview(imageViewQR)
        imageViewQR.snp.makeConstraints { (make) in
            make.top.equalTo(bottomLine.snp_bottom).offset(5)
            make.right.equalTo(bottomLine.snp_right).offset(-20)
            make.height.equalTo(60)
            make.width.equalTo(60)
//            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15))
        }
        //二维码里的logo
        let QRLogoImageView = UIImageView()
        QRLogoImageView.layer.cornerRadius  =  4
        QRLogoImageView.layer.masksToBounds = true
        imageViewQR.addSubview(QRLogoImageView)
        QRLogoImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(20)
            make.center.equalToSuperview()
        }
        if model.QRType == "0"{
            QRLogoImageView.image = UIImage.init(named: "V")
        }else if model.QRType == "1"{
            QRLogoImageView.image = UIImage.init(named: "G")
        }
        if model.language == "0"{
            companyLabel.text = "公司"
            name1Label.text = "访客"
            name2Label.text = "受访人"
            addressLabel.text = "访问工厂"
            dateLabel.text = "访问日期"
            timeLabel.text = "访问时间"
        }
        
//        print("当前系统时间 == \(time)")
//
//        if model.language == "0"{
//
//            labelConfig(addressLabel, str: "访问工厂 : \(model.address)")
//            labelConfig(name1Label,   str: "访客   : \(model.name1)")
//            //labelConfig(typeLabel,    str: "访问类型 : ")
//            labelConfig(name2Label,   str: "受访人 : \(model.name2)")
//            labelConfig(timeLabel,    str: "访问时间 : \(time)")
//            labelConfig(idLabel,      str: "ID : \(model.visterId)")
//        }else{
//
//            labelConfig(addressLabel, str: "Visited factory  : \(model.address)")
//            labelConfig(name1Label,   str: "Visitor name : \(model.name1)")
//            //labelConfig(typeLabel,    str: "Visitor type      : ")
//            labelConfig(name2Label,   str: "Host name       : \(model.name2)")
//            labelConfig(timeLabel,    str: "Visit time   : \(time)")
//            labelConfig(idLabel,      str: "ID : \(model.visterId)")
//        }
//
       
        
        
//        var i = 0
     
//        labelArray.forEach { (label) in
//
//            label.snp.makeConstraints({ (make) in
//                make.left.equalTo(15)
//                make.top.equalTo(80 + 60 * i)
//            })
//
//            let line                = UIView()
//            line.backgroundColor    = .gray
//            printView.addSubview(line)
//            line.snp.makeConstraints({ (make) in
//                make.left.equalTo(15)
//                make.top.equalTo(110 + 60 * i)
//                make.width.equalTo(280)
//                make.height.equalTo(0.5)
//            })
//
//            i = i + 1
//        }
        
        //判断图片
//        if model.iconType == "0" {
//            typeImageView.image = UIImage.init(named: "adidas")
//        }else if model.iconType == "1" {
//            typeImageView.image = UIImage.init(named: "Jordan")
//        }else if model.iconType == "2" {
//            typeImageView.image = UIImage.init(named: "nike")
//        }else if model.iconType == "3" {
//            typeImageView.image = UIImage.init(named: "PUMA")
//        }else if model.iconType == "4" {
//            typeImageView.image = UIImage.init(named: "other")
//        }
//
//
//
//        printView.addSubview(typeImageView)
//        typeImageView.snp.makeConstraints { (make) in
//            make.left.equalTo(typeLabel.snp.right).offset(7)
//            make.width.equalTo(60)
//            make.height.equalTo(40)
//            make.centerY.equalTo(typeLabel.snp.centerY)
//        }
        
//        printView.addSubview(idLabel)
//        idLabel.snp.makeConstraints { (make) in
//            make.centerX.equalToSuperview()
//            make.bottom.equalTo(-15)
//        }
//
    }
    
    func labelConfig(_ label: UILabel, str: String){
         label.font = UIFont.systemFont(ofSize: 21)
//        if label == name1Label || label == idLabel {
//            //label.font = UIFont.systemFont(ofSize: 28)
//            label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
//        }else{
//            label.font = UIFont.systemFont(ofSize: 21)
//        }
       
        label.text = str
    }
    
    func currentTime() -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "HH:mm"// 自定义时间格式
           // GMT时间 转字符串，直接是系统当前时间
        return dateformatter.string(from: Date())
    }
    func currentDate() -> String {
           let dateformatter = DateFormatter()
           dateformatter.dateFormat = "YYYY/MM/dd"// 自定义时间格式
              // GMT时间 转字符串，直接是系统当前时间
           return dateformatter.string(from: Date())
       }
}

