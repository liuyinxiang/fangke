//
//  ShowView.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/28.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class ShowView: UIView {
 
    var topType    : Bool        = false
    
    let label = UILabel()
    
    let imageView = UIImageView()
    
    var theIndex : String = ""{
        didSet {
            label.text = "MUJI - \(theIndex)"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configUI() {
        
       
        let background = UIView()
        background.backgroundColor = .randomColor
        self.addSubview(background)
     
        label.text = "dede"
        label.textColor = .white
        label.font = UIFont.HeitiSC(15)
        self.addSubview(label)
        
        background.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(screenWidth / 2 - 50)
        }
 
    
    
        background.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        background.isUserInteractionEnabled = true
        background.layer.masksToBounds = true
        
        let clickTap = UITapGestureRecognizer(target: self, action: #selector(ShowView.clikeTap(_:)))
        background.addGestureRecognizer(clickTap)
        
       
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -4)
        imageView.contentMode = .scaleAspectFill
      
        background.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(screenWidth / 2 + 10)
        }
        
        
        label.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
    }
    
    @objc func clikeTap(_ sender: UITapGestureRecognizer) {
        uLog("text == \(String(describing: label.text))")
    }
    
}
