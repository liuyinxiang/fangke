//
//  AnimateViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/6.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class AnimateViewController: FullTitleVisualEffectViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let gakkiImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!))
        gakkiImageView.contentMode = .scaleAspectFill
        gakkiImageView.image = UIImage.init(named: "Gakki")
        contentView?.addSubview(gakkiImageView)
        
        let label = UILabel(frame: CGRect(x: 100, y: 200, width: 200, height: 30))
        label.text = "123"
        contentView?.addSubview(label)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of
            label.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.5, options: UIView.AnimationOptions(),
                           animations: {
                            label.transform = CGAffineTransform.identity
                            label.text = "9921"
            }, completion: nil)
        }
    }
    
    

  
}
