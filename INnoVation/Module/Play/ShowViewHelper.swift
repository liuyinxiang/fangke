//
//  ShowViewHelper.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/29.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import Foundation


extension ShowViewController {
     
    @objc func makeSwipeAction(_ sender: UISwipeGestureRecognizer) {
        
        switch sender.direction {
        case .right:
            uLog("是right")
            imageChange()
            snakeRight()
        case .left:
            uLog("是left")
            imageChange()
            snakeLeft()
        default: break
        }
    }
    
    func imageChange() {
 
        let transition = CATransition()
        transition.duration = 0.7;
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade;
        imageView.layer.add(transition, forKey: "a")
        if imagechangeMark == true{
            imageView.image? = UIImage.init(named: "Bird")!
            imagechangeMark = !imagechangeMark
        }else{
            imageView.image? = UIImage.init(named: "Gakki")!
            imagechangeMark = !imagechangeMark
        }
        
 
    }
    
    
    func snakeRight(){
        
        if midIndex == 0{
            midIndex = self.titleArray.count
        }
        uLog("midIndex == \(midIndex)")
        
        //处理无限滚动 最右的view 放到 左边
        let rightView = viewArray.last
        let markView = viewArray[1]
        rightView?.frame = markView.frame
        rightView?.frame.origin.x = markView.frame.origin.x - showViewW
        rightView?.topType = markView.topType
        
        viewArray.remove(at: viewArray.count - 1)
        viewArray.insert(rightView!, at: 0)
        
        for i in 0..<titleArray.count {
            
            let oneShowView = contentView?.viewWithTag(i + viewTag) as! ShowView
            var newFrame = oneShowView.frame
            //右移
            newFrame.origin.x = newFrame.origin.x + self.showViewW / 2
            
            if oneShowView.topType == true{
                
                if self.changeMark {
                    newFrame.origin.y = newFrame.origin.y + self.showViewW / 2
                }else{
                    newFrame.origin.y = newFrame.origin.y - self.showViewW / 2
                }
                
                UIView.animate(withDuration: 0.4) {
                    
                    if  oneShowView.tag  == self.midIndex + self.viewTag - 1 {
                        oneShowView.alpha = 1
                    }else{
                        oneShowView.alpha = 0.4
                    }
                    
                    oneShowView.frame = newFrame
                }
            }else{
                
                if self.changeMark {
                    newFrame.origin.y = newFrame.origin.y - self.showViewW / 2
                }else{
                    newFrame.origin.y = newFrame.origin.y + self.showViewW / 2
                }
                
                UIView.animate(withDuration: 0.4) {
                    
                    if  oneShowView.tag == self.midIndex + self.viewTag - 1 {
                        oneShowView.alpha = 1
                    }else{
                        oneShowView.alpha = 0.4
                    }
                    
                    oneShowView.frame = newFrame
                }
            }
        }
        
        //中间index - 1
        midIndex = midIndex - 1
        
        self.changeMark = !self.changeMark
    }
    
    func snakeLeft(){
        
        if midIndex == self.titleArray.count - 1 {
            midIndex = -1
        }
        
        //处理无限滚动 最左的view 放到 右边
        let leftView = viewArray.first
        let markView = viewArray[viewArray.count - 2]
        leftView?.frame = markView.frame
        leftView?.frame.origin.x = markView.frame.origin.x + showViewW
        leftView?.topType = markView.topType
        
        viewArray.remove(at: 0)
        viewArray.append(leftView!)
        
        
        for i in 0..<titleArray.count {
            
            let oneShowView = contentView?.viewWithTag(i + viewTag) as! ShowView
            var newFrame = oneShowView.frame
            
            //左移
            newFrame.origin.x = newFrame.origin.x - self.showViewW / 2
            
            
            if oneShowView.topType == true{
                
                if self.changeMark {
                    newFrame.origin.y = newFrame.origin.y + self.showViewW / 2
                }else{
                    newFrame.origin.y = newFrame.origin.y - self.showViewW / 2
                }
                
                UIView.animate(withDuration: 0.4) {
                    
                    if  oneShowView.tag  == self.midIndex + self.viewTag + 1 {
                        oneShowView.alpha = 1
                    }else{
                        oneShowView.alpha = 0.4
                    }
                    
                    oneShowView.frame = newFrame
                }
            }else{
                
                if self.changeMark {
                    newFrame.origin.y = newFrame.origin.y - self.showViewW / 2
                }else{
                    newFrame.origin.y = newFrame.origin.y + self.showViewW / 2
                }
                
                UIView.animate(withDuration: 0.4) {
                    
                    if  oneShowView.tag  == self.midIndex + self.viewTag + 1 {
                        oneShowView.alpha = 1
                    }else{
                        oneShowView.alpha = 0.4
                    }
                    
                    oneShowView.frame = newFrame
                }
            }
            
        }
        //中间index + 1
        midIndex = midIndex + 1
        self.changeMark = !self.changeMark
    }
}
