//
//  GakkiViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/3.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class GakkiView: UIView {
    
    var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configUI() {
        
        let background = UIView()
        background.backgroundColor = .theme
        background.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        background.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4) // 旋转动画, 旋转pi
        background.isUserInteractionEnabled = true
        background.layer.masksToBounds = true
        self.addSubview(background)
        
        
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -4)
        imageView.contentMode = .scaleAspectFill
        
        background.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(screenWidth / 2 + 10)
        }
        
    }
    
}

class GakkiViewController: FullTitleVisualEffectViewController {

    let view1 = GakkiView()
    let view2 = GakkiView()
    let view3 = GakkiView()
    let W = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //title = "Gakki"
        
        creatUI()
    }
 
 
    func creatUI() {
     
//        let gakkiImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!))
//        gakkiImageView.contentMode = .scaleAspectFill
//        gakkiImageView.image = UIImage.init(named: "Gakki")
//        contentView?.addSubview(gakkiImageView)
        
        contentView?.backgroundColor = .black
        
        contentView?.sd_addsubViews(subviews: [view1, view2, view3])
        
        
        layoutUI()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        layoutUI()
       
    }
    
    func layoutUI() {
        
        view1.frame = CGRect(x: -300, y: 170, width: W, height: W)
        view1.alpha = 0
        view1.imageView.image = UIImage.init(named: "muji1")
        
        view2.frame = CGRect(x: -300, y: 280, width: W, height: W)
        view2.alpha = 0
        view2.imageView.image = UIImage.init(named: "muji2")
        
        view3.frame = CGRect(x: -300, y: 390, width: W, height: W)
        view3.alpha = 0
        view3.imageView.image = UIImage.init(named: "muji3")
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let duration: TimeInterval = 1.4 // 动画持续时间
        let delay: TimeInterval = 0 // 动画延迟时间
        let options: UIView.AnimationOptions = .curveEaseInOut // 动画选项
        let damping: CGFloat = 0.77 // 阻尼. 越大感觉越有粘性
        let velocity: CGFloat = 0 // 初速度
      
        UIView.animate(withDuration: duration, delay: delay, usingSpringWithDamping: damping, initialSpringVelocity: velocity, options: options, animations: {
          
            self.view1.alpha = 0.9
            self.view1.frame.origin.x = 80
            //self.view1.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2) // 旋转动画, 旋转pi
          
            
        }) { (finish) in}
        
        UIView.animate(withDuration: duration, delay: delay + 0.5, usingSpringWithDamping: damping, initialSpringVelocity: velocity, options: options, animations: {
            
            self.view2.alpha = 0.9
            self.view2.frame.origin.x = 150
            
            
        }) { (finish) in}
        
        UIView.animate(withDuration: duration, delay: delay + 1, usingSpringWithDamping: damping, initialSpringVelocity: velocity, options: options, animations: {
            
            self.view3.alpha = 0.9
            self.view3.frame.origin.x = 220
            
        }) { (finish) in}
    }
}
