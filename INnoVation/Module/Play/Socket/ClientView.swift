//
//  ClientView.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/4.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import Foundation

extension ClientViewController {
    func creatUI() {
        
        let gakkiImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!))
        gakkiImageView.contentMode = .scaleAspectFill
        gakkiImageView.image = UIImage.init(named: "Gakki")
        contentView?.addSubview(gakkiImageView)
        
        let label1 = UILabel(frame: CGRect(x: 8, y: 150 + 20, width: 50, height: 35))
        label1.textColor = .white
        label1.text = "端口:"
        contentView?.addSubview(label1)
        
        portTF.frame = CGRect(x: 55, y: 150 + 20, width: 200, height: 35)
        portTF.backgroundColor = .white
        portTF.alpha = 0.7
        contentView?.addSubview(portTF)
        
        let label = UILabel(frame: CGRect(x: 8, y: 100 + 20, width: 50, height: 35))
        label.textColor = .white
        label.text = "IP:"
        contentView?.addSubview(label)
        
        ipTF.frame = CGRect(x: 55, y: 100 + 20 , width: 200, height: 35)
        ipTF.backgroundColor = .white
        ipTF.alpha = 0.7
        contentView?.addSubview(ipTF)
        
        
        let btn = UIButton(frame: CGRect(x: 280, y: 100 + 20, width: 70, height: 35))
        btn.backgroundColor = .white
        btn.setTitleColor(.black, for: .normal)
        btn.setTitle("连接", for: .normal)
        btn.addTarget(self, action: #selector(ClientViewController.connectionAct(sender:)), for: .touchUpInside)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn.layer.cornerRadius = 4
        contentView?.addSubview(btn)
        
        let btn1 = UIButton(frame: CGRect(x: 280, y: 150 + 20, width: 70, height: 35))
        btn1.backgroundColor = .white
        btn1.setTitleColor(.black, for: .normal)
        btn1.setTitle("断开", for: .normal)
        btn1.addTarget(self, action: #selector(ClientViewController.disconnectAct(sender:)), for: .touchUpInside)
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn1.layer.cornerRadius = 4
        contentView?.addSubview(btn1)
        
        let btn2 = UIButton(frame: CGRect(x: 280, y: 200 + 20 , width: 70, height: 35))
        btn2.backgroundColor = .white
        btn2.setTitleColor(.black, for: .normal)
        btn2.setTitle("发送", for: .normal)
        btn2.addTarget(self, action: #selector(ClientViewController.sendMsgAct(sender:)), for: .touchUpInside)
        btn2.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn2.layer.cornerRadius = 4
        contentView?.addSubview(btn2)
        
        let label2 = UILabel(frame: CGRect(x: 8, y: 200 + 20, width: 50, height: 35))
        label2.textColor = .white
        label2.text = "消息:"
        contentView?.addSubview(label2)
        
        msgTF.frame = CGRect(x: 55, y: 200 + 20, width: 200, height: 35)
        msgTF.backgroundColor = .white
        msgTF.alpha = 0.7
        contentView?.addSubview(msgTF)
        
        infoTV.frame = CGRect(x: 50, y: 250 + 20, width: 280, height: 350)
        infoTV.backgroundColor = .white
        infoTV.alpha = 0.7
        contentView?.addSubview(infoTV)
    }
}
