//
//  ClientViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/4.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import CocoaAsyncSocket

class ClientViewController: FullTitleVisualEffectViewController {

    //IP地址
    let ipTF    = UITextField()
    //端口
    let portTF  = UITextField()
    //消息
    let msgTF   = UITextField()
    //信息显示
    let infoTV  = UITextView()
    
    var socket: GCDAsyncSocket?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creatUI()
        
    }
    
    //对InfoTextView添加提示内容
    func addText(text: String) {
        infoTV.text = infoTV.text.appendingFormat("%@\n", text)
    }
    
    func feedBack(){
        if #available(iOS 10.0, *) {
            let feedBackGenertor = UIImpactFeedbackGenerator(style: .light)
            feedBackGenertor.impactOccurred()
        }
    }
        
  
    //连接
    @objc func connectionAct(sender: AnyObject) {
        feedBack()
         
        guard let port = UInt16(portTF.text!),
              let ip   = ipTF.text
        else {
            print("IP/端口不能为空")
            return
        }
        
        socket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
         
        do {
            try socket?.connect(toHost: ip, onPort: port)
            addText(text: "连接成功")
        }catch _ {
            addText(text: "连接失败")
        }
    }
    
    //断开
    @objc func disconnectAct(sender: AnyObject) {
        feedBack()
        socket?.disconnect()
        addText(text: "断开连接")
    }
    
    //发送
    @objc func sendMsgAct(sender: AnyObject) {
        feedBack()
        
        addText(text: "我: \(msgTF.text ?? "")")
        socket?.write((msgTF.text?.data(using: String.Encoding.utf8))!, withTimeout: -1, tag: 0)
    }
    
}

extension ClientViewController: GCDAsyncSocketDelegate {
    
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        addText(text: "连接服务器" + host)
        self.socket?.readData(withTimeout: -1, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        var msg = String(data: data as Data, encoding: String.Encoding.utf8)
        msg = "S:\(msg ?? "")"
        addText(text: msg!)
        socket?.readData(withTimeout: -1, tag: 0)
    }
    
}

