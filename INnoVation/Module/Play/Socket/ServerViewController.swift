//
//  ServerViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/4.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import CocoaAsyncSocket

class ServerViewController: FullTitleVisualEffectViewController {

    //端口
    let portTF = UITextField()
    
    //消息
    let msgTF  = UITextField()
    
    //信息显示
    let infoTV = UITextView()
    
    //服务端和客户端的socket引用
    var serverSocket: GCDAsyncSocket?
    var clientSocket: GCDAsyncSocket?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        creatUI()
        
    }
    
    func feedBack(){
        if #available(iOS 10.0, *) {
            let feedBackGenertor = UIImpactFeedbackGenerator(style: .light)
            feedBackGenertor.impactOccurred()
        }
    }
    
    //对InfoTextView添加提示内容
    func addText(text: String) {
        infoTV.text = infoTV.text.appendingFormat("%@\n", text)
    }
    
    ///监听
    @objc func listeningAct(sender: AnyObject) {
        
        feedBack()
        serverSocket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
        
        guard let text = UInt16(portTF.text!) else {
            print("端口不能为空")
            return
        }
     
        do {
            try serverSocket?.accept(onPort: text)
            addText(text: "监听成功")
        }catch _ {
            addText(text: "监听失败")
        }
 
        
        
    }
    
    ///发送
    @objc func sendAct(sender: AnyObject) {
        feedBack()
        
        addText(text: "我: \(msgTF.text ?? "")")
        
        let data = msgTF.text?.data(using: String.Encoding.utf8)
        //向客户端写入信息   Timeout设置为 -1 则不会超时, tag作为两边一样的标示
        clientSocket?.write(data!, withTimeout: -1, tag: 0)
    }
    
}


extension ServerViewController: GCDAsyncSocketDelegate{
    
    //当接收到新的Socket连接时执行
    func socket(_ sock: GCDAsyncSocket, didAcceptNewSocket newSocket: GCDAsyncSocket) {
        
        addText(text: "连接成功")
        addText(text: "连接地址" + newSocket.connectedHost!)
        addText(text: "端口号" + String(newSocket.connectedPort))
        clientSocket = newSocket
        
        //第一次开始读取Data
        clientSocket!.readData(withTimeout: -1, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        var message = String(data: data as Data,encoding: String.Encoding.utf8)
        message = "C:\(message ?? "")"
        addText(text: message!)
        //再次准备读取Data,以此来循环读取Data
        sock.readData(withTimeout: -1, tag: 0)
    }
    
   
    
}
