//
//  ServerView.swift
//  INnoVation
//
//  Created by INnoVation on 2018/9/4.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import Foundation

extension ServerViewController {
     
    func creatUI() {
        
        let gakkiImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!))
        gakkiImageView.contentMode = .scaleAspectFill
        gakkiImageView.image = UIImage.init(named: "Bird")
        contentView?.addSubview(gakkiImageView)
        
        
        let label1 = UILabel(frame: CGRect(x: 8, y: 150, width: 50, height: 35))
        label1.textColor = .white
        label1.text = "端口:"
        contentView?.addSubview(label1)
        
        portTF.frame = CGRect(x: 55, y: 150, width: 200, height: 35)
        portTF.backgroundColor = .white
        portTF.alpha = 0.7
        contentView?.addSubview(portTF)
        
        let btn1 = UIButton(frame: CGRect(x: 280, y: 150, width: 70, height: 35))
        btn1.backgroundColor = .white
        btn1.setTitleColor(.black, for: .normal)
        btn1.setTitle("监听", for: .normal)
        btn1.addTarget(self, action: #selector(ServerViewController.listeningAct(sender:)), for: .touchUpInside)
        btn1.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn1.layer.cornerRadius = 4
        contentView?.addSubview(btn1)
        
        let btn2 = UIButton(frame: CGRect(x: 280, y: 200, width: 70, height: 35))
        btn2.backgroundColor = .white
        btn2.setTitleColor(.black, for: .normal)
        btn2.setTitle("发送", for: .normal)
        btn2.addTarget(self, action: #selector(ServerViewController.sendAct(sender:)), for: .touchUpInside)
        btn2.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btn2.layer.cornerRadius = 4
        contentView?.addSubview(btn2)
        
        let label2 = UILabel(frame: CGRect(x: 8, y: 200, width: 50, height: 35))
        label2.textColor = .white
        label2.text = "消息:"
        contentView?.addSubview(label2)
        
        msgTF.frame = CGRect(x: 55, y: 200, width: 200, height: 35)
        msgTF.backgroundColor = .white
        msgTF.alpha = 0.7
        contentView?.addSubview(msgTF)
        
        infoTV.frame = CGRect(x: 50, y: 250, width: 280, height: 350)
        infoTV.backgroundColor = .white
        infoTV.alpha = 0.7
        contentView?.addSubview(infoTV)
    }
    
}
