//
//  RxViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/23.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RxViewController: NormalTitleViewController {

    let disposeBag = DisposeBag() //用于管理Observable
    
     override func viewDidLoad() {
        super.viewDidLoad()
        view.sd_addsubViews(subviews: [userNameField, passwordField, loginButton, passwordField2])
        
        let usernameValid = userNameField.rx.text.orEmpty
            .map { $0.count >= 11 }
            .share(replay: 1)
        
        let passwordValid = passwordField.rx.text.orEmpty
            .map { $0.count >= 11 }
            .share(replay: 1)
        
        let passwordValid2 = passwordField2.rx.text.orEmpty
            .map { $0.count >= 11 }
            .share(replay: 1)
        
        // 所有输入是否有效
        let everythingValid = Observable.combineLatest(
            usernameValid,
            passwordValid,
            passwordValid2
        ) { $0 && $1 && $2 } // 取用户名和密码同时有效
            .share(replay: 1)
             
        // 所有输入是否有效 -> 绿色按钮是否可点击
        everythingValid
            .filter{ $0 == true }
            .bind { [weak self](text) in
                //收到发出的索引数后显示到label上
               // self?.label.text = text
                uLog("text == \(text)")
            }
            .disposed(by: disposeBag)

        
        //.bind(to: loginButton.rx.isEnabled)
        
        
        let imageViewIcon1 = UIImageView(frame: CGRect(x:20, y:200, width:150, height:150))
        imageViewIcon1.image = createQRForString(qrString: "http://www.hangge.com",
                                                 qrImageName: "")
        contentView?.addSubview(imageViewIcon1)
        
        
    }
    
    //创建二维码图片
    func createQRForString(qrString: String?, qrImageName: String?) -> UIImage?{
        if let sureQRString = qrString {
            let stringData = sureQRString.data(using: .utf8,
                                               allowLossyConversion: false)
            // 创建一个二维码的滤镜
            let qrFilter = CIFilter(name: "CIQRCodeGenerator")!
            qrFilter.setValue(stringData, forKey: "inputMessage")
            qrFilter.setValue("H", forKey: "inputCorrectionLevel")
            let qrCIImage = qrFilter.outputImage
            
            // 创建一个颜色滤镜,黑白色
            let colorFilter = CIFilter(name: "CIFalseColor")!
            colorFilter.setDefaults()
            colorFilter.setValue(qrCIImage, forKey: "inputImage")
            colorFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor0")
            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1")
            
            // 返回二维码image
            let codeImage = UIImage(ciImage: colorFilter.outputImage!
                .transformed(by: CGAffineTransform(scaleX: 5, y: 5)))
            
            // 通常,二维码都是定制的,中间都会放想要表达意思的图片
            if let iconImage = UIImage(named: qrImageName!) {
                let rect = CGRect(x:0, y:0, width:codeImage.size.width,
                                  height:codeImage.size.height)
                UIGraphicsBeginImageContext(rect.size)
                
                codeImage.draw(in: rect)
                let avatarSize = CGSize(width:rect.size.width * 0.25,
                                        height:rect.size.height * 0.25)
                let x = (rect.width - avatarSize.width) * 0.5
                let y = (rect.height - avatarSize.height) * 0.5
                iconImage.draw(in: CGRect(x:x, y:y, width:avatarSize.width,
                                          height:avatarSize.height))
                let resultImage = UIGraphicsGetImageFromCurrentImageContext()
                
                UIGraphicsEndImageContext()
                return resultImage
            }
            return codeImage
        }
        return nil
    }
    
 
    lazy var userNameField: UITextField = {
        let make = UITextField(frame: CGRect(x: 40, y: 100, width: 200, height: 40))
        make.placeholder = "用户名"
        return make
    }()

    lazy var passwordField: UITextField = {
        let make = UITextField(frame: CGRect(x: 40, y: 160, width: 200, height: 40))
        make.placeholder = "密码"
        return make
    }()
    
    lazy var passwordField2: UITextField = {
        let make = UITextField(frame: CGRect(x: 40, y: 260, width: 200, height: 40))
        make.placeholder = "密码"
        return make
    }()
    
    lazy var loginButton: UIButton = {
        let make = UIButton(frame: CGRect(x: 40, y: 200, width: 70, height: 40))
        make.setTitle("登录", for: .normal)
        make.setTitleColor(.green, for: .normal)
        make.setTitleColor(.red, for: .disabled)
        
        return make
    }()
}
