//
//  iCarouselViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/29.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class iCarouselViewController: FullTitleVisualEffectViewController, iCarouselDataSource, iCarouselDelegate {

    var carousel = iCarousel(frame: CGRect(x: 0, y: 250, width: screenWidth, height: 300))
    
    var items: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for i in 0 ... 99 {
            items.append(i)
        }
        
        title = "iCarousel"
        contentView?.backgroundColor = .white
        
        
        carousel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / -2)
        carousel.dataSource = self;
        carousel.delegate = self;
        carousel.type = .wheel;
        contentView?.addSubview(carousel)
        // Do any additional setup after loading the view.
    }
  
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        var itemView: UIImageView
        
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
            //get a reference to the label in the recycled view
            label = itemView.viewWithTag(1) as! UILabel
        } else {
            //don't do anything specific to the index within
            //this `if ... else` statement because the view will be
            //recycled and used with other index values later
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 150))
            itemView.image = UIImage(named: "muji6")
            itemView.contentMode = .scaleAspectFill
            
            
            label = UILabel(frame: itemView.bounds)
            label.backgroundColor = .clear
            label.textAlignment = .center
            label.font = label.font.withSize(50)
            label.tag = 1
            itemView.addSubview(label)
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        label.text = "\(items[index])"
        
        itemView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
}
