//
//  BlueViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/21.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class BlueViewController: FullTitleVisualEffectViewController {

    
    var titlelabelHeroId : String = ""
    var subTitleLabelHeroId : String = ""
    var theimageViewHeroId : String = ""
    
    var titlelabel    : UILabel!
    var subTitleLabel : UILabel!
    var theimageView  : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView?.backgroundColor = UIColor(red:0.18, green:0.58, blue:0.97, alpha:1.00)
        titleView?.isHidden = true
        // Do any additional setup after loading the view.
        
        titlelabel      = UILabel(frame: CGRect(x: 100, y: 78, width: 290, height: 25))
        titlelabel.font = UIFont.HeitiSC(16)
        titlelabel.text = "naidoansd"
        //titlelabel.hero.id = titlelabelHeroId
        contentView?.addSubview(titlelabel)
        
        subTitleLabel           = UILabel(frame: CGRect(x: 100, y: 105, width: 290, height: 10))
        subTitleLabel.font      = UIFont.AvenirLight(8)
        subTitleLabel.text = "naidoansd"
        //subTitleLabel.hero.id = subTitleLabelHeroId
        subTitleLabel.textColor = UIColor.gray
        contentView?.addSubview(subTitleLabel)
        
        
        theimageView = UIImageView()
        //theimageView.hero.id = theimageViewHeroId
        theimageView.image = UIImage.init(named: "0135")
       // theimageView.isSkeletonable               = true
       // contentView?.isSkeletonable                = true
        contentView?.addSubview(theimageView)
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //_ = setupOnceSkeleton
         //contentView?.showAnimatedGradientSkeleton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        theimageView.frame = CGRect(x: 150, y: 200, width: 130, height: 130)
        
       // uLog("vc imageheroID = \(String(describing: theimageView.hero.id))")
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.navigationController?.pushViewController(BlueViewController(), animated: true)
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
