//
//  RedViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/21.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import AudioToolbox
import PromiseKit

class RedViewController: NormalTitleViewController, UITableViewDataSource, UITableViewDelegate, CustomCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate  {
   
    fileprivate var tableView    : UITableView!
    fileprivate var notification : DefaultNotificationCenter!
    fileprivate var adapters     : [CellDataAdapter] = [CellDataAdapter]()
    
    fileprivate var titlelabel    : UILabel!
    fileprivate var subTitleLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentView?.backgroundColor = UIColor(red:0.88, green:0.29, blue:0.29, alpha:1.00)
        
//        view.isSkeletonable                    = true
//        backgroundView?.isSkeletonable         = true
//        contentView?.isSkeletonable            = true
//
        // TableView.
        tableView                              = UITableView(frame: (contentView?.bounds)!)
        tableView.dataSource                   = self
        tableView.delegate                     = self
        tableView.separatorStyle               = .none
        tableView.rowHeight                    = 50
        tableView.showsVerticalScrollIndicator = false
        tableView.emptyDataSetSource           = self
        tableView.emptyDataSetDelegate         = self
       // tableView.isSkeletonable               = true
        contentView?.addSubview(tableView)
        
  
        
        titlelabel      = UILabel(frame: CGRect(x: 50, y: 125, width: 290, height: 10))
        titlelabel.font = UIFont.HeitiSC(16)
        titlelabel.text = "naidoansd"
//        titlelabel.hero.id = "label1"
        contentView?.addSubview(titlelabel)
        
        subTitleLabel           = UILabel(frame: CGRect(x: 200, y: 205, width: 290, height: 10))
        subTitleLabel.font      = UIFont.AvenirLight(8)
        subTitleLabel.text = "naidoansd"
//        subTitleLabel.hero.id = "label2"
        subTitleLabel.textColor = UIColor.gray
        contentView?.addSubview(subTitleLabel)
        
        // Add
        
        func add(_ name1 : String!, name2 : String!) {
            
            let model = RedModel()
            model.name1 = name1
            model.name2 = name2
            
            adapters.append(RedCell.Adapter(data: model ))
        }
        
        func add2(_ name1 : String!, name2 : String!) {
            
            let model = RedModel()
            model.name1 = name1
            model.name2 = name2
            
            adapters.append(BlueCell.Adapter(data: model))
        }
        add("dede0", name2: "enen")
        add("dede1", name2: "enen")
        add("dede2", name2: "enen")
        add("dede3", name2: "enen")
        add("dede4", name2: "enen")
        add("dede5", name2: "enen")
        add("naidoansd", name2: "naidoansd")
        add2("naidoansd", name2: "naidoansd")
        add2("naidoansd", name2: "naidoansd")
        add2("naidoansd", name2: "naidoansd")
        add2("naidoansd", name2: "naidoansd")
 
        // Register cell.
        RedCell.RegisterTo(tableView)
        BlueCell.RegisterTo(tableView)
        
//        let header = URefreshDIYHeader()// 顶部刷新
//        header.setRefreshingTarget(self, refreshingAction: #selector(loadData))
//        tableView.mj_header = header
        
     
    }
 
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         //_ = setupOnceSkeleton
    }
    
    lazy var setupOnceSkeleton: Void = {
        //tableView.mj_header.beginRefreshing()
       // contentView?.showAnimatedSkeleton()
        //headerview.showAnimatedSkeleton()
        DispatchQueue.main.asyncAfter(deadline: .now()+3, execute:
            {
              //  self.contentView?.hideSkeleton()
                self.tableView.reloadData()
        })
    }()
    
    @objc func loadData() {

//        firstly {
//            CallApi(KJoeApi.channels)
//        }.then { (result) -> Promise<String> in
//
//            if let cats = [RedModel].deserialize(from: result) {
//                cats.forEach({ (cat) in
//                    uLog("cat?.name == \(String(describing: cat?.name))")
//
//                    self.adapters.append(RedCell.Adapter(data: cat))
//                })
//            }
//
//            self.tableView.reloadData()
//            self.tableView.mj_header.endRefreshing()
//
//            CallApi(KJoeApi.playlist("拉丁"))
//        }.then { (result) -> Promise<String> in
//
//            CallApi(KJoeApi.playlist("拉丁"))
//        }.catch {_ in
//            //…
//        }

        firstly {
            
            CallApi(KJoeApi.channels)
            }.then { (result) -> Promise<String> in
                
                self.adapters.removeAll()
            
                guard let cats = [RedModel].deserialize(from: result) else {
                    return Promise.init(error: NSError(domain: "用于Promise-moya里的失败", code: 0, userInfo: nil))
                }
                
                let cat = cats.last ?? RedModel.init()
                let thenStr = cat?.name ?? ""
                uLog("thenStr == \(thenStr)")
                uLog("第1个")
                return CallApi(KJoeApi.playlist(thenStr))
            }.then { (result) -> Promise<String> in
                uLog("第2个")
                return CallApi(KJoeApi.playlist("拉丁"))
            }.done { _ in
                uLog("第3个")
            }.catch {  (error) -> Void in
                uLog("第4个")
                print("error == \(error)")
                self.tableView.mj_header.endRefreshing()
                // self?.show(UIAlertController(coder: error as! NSCoder)!, sender: self)
        }

    }
    
    // MARK: tableview Delegate.
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return adapters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return  tableView.dequeueCellAndLoadContentFromAdapter(adapters[indexPath.row], indexPath: indexPath, delegate : self)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.selectedEventWithIndexPath(indexPath)
    }
    
    func customCell(_ cell: CustomCell?, event: Any?) {
     
        let label1heroID = "label1_\(String(describing: cell?.indexPath?.row))"
        let label2heroID = "label2_\(String(describing: cell?.indexPath?.row))"
        let imageheroID = "image_\(String(describing: cell?.indexPath?.row))"
   
        let vc = BlueViewController()
        
       // vc.hero.isEnabled = true
        vc.titlelabelHeroId          = label1heroID
        vc.subTitleLabelHeroId       = label2heroID
        vc.theimageViewHeroId        = imageheroID
        
        //navigationController?.hero.isEnabled = true
        
        //vc.hero.modalAnimationType = .selectBy(presenting: .pull(direction: .left), dismissing: .slide(direction: .down))
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        
        return UIImage(named: "0135")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let emptyString = "Swift"
        let emptyAttribute: [NSAttributedString.Key: Any] = [
            .strokeColor : UIColor.black,
            .foregroundColor : UIColor.white,
            .strokeWidth : -2.0,
            .font : UIFont.boldSystemFont(ofSize: 18)
        ]
        return  NSAttributedString(string: emptyString, attributes: emptyAttribute)
    }
    
    
    
    
}
