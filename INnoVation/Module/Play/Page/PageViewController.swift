//
//  PageViewController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/31.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class PageViewController: NormalTitleViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let root = WMPageRootController()
       
        root.titleSizeNormal = 14;
        root.titleSizeSelected = 14;
        root.menuViewStyle = .line;
        root.menuItemWidth = screenWidth / CGFloat(2 + 1);
        root.progressWidth = 50;
        root.titleColorSelected = .red;
        root.titleColorNormal = .black
        root.menuViewLayoutMode = .center;
        
        addChild(root)
        root.didMove(toParent: self)
        root.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: (contentView?.height)!)
        contentView?.addSubview(root.view)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
}
