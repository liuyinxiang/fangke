//
//  WMPageRootController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/31.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class WMPageRootController: WMPageController {

    let titleData : [String] = ["的的", "嗯嗯"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //view.backgroundColor = .blue

        
        
    }

    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    // menu
    override func pageController(_ pageController: WMPageController, preferredFrameFor menuView: WMMenuView) -> CGRect {
        return CGRect(x: 0, y: 0, width: screenWidth, height: 40)
    }
    
    override func pageController(_ pageController: WMPageController, titleAt index: Int) -> String {
        return titleData[index]
    }

    override func numbersOfChildControllers(in pageController: WMPageController) -> Int {
        return titleData.count
    }

    
    // view
    override func pageController(_ pageController: WMPageController, preferredFrameForContentView contentView: WMScrollView) -> CGRect {
        return CGRect(x: 0, y: 60, width: screenWidth, height: screenHeight - 200)
    }
    
    override func pageController(_ pageController: WMPageController, viewControllerAt index: Int) -> UIViewController {
        switch index {
        case 0:
            return BlueViewController()
        case 1:
            return BlueViewController()
        default:
            return UIViewController()
        }
    }
    
    

   
   
}
