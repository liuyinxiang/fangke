//
//  RedCell.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/22.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class RedCell: CustomCell {

    // MARK: Private var.
    
      var titlelabel    : UILabel!
      var subTitleLabel : UILabel!
      var theimageView     : UIImageView!
    
    // MARK: Override CustomCell func.
    
    override func setupCell() {
        
        super.setupCell()
        self.accessoryType = .disclosureIndicator
    }
    
    override func buildSubview() {
        
        titlelabel      = UILabel(frame: CGRect(x: 10, y: 8, width: 290, height: 25))
        titlelabel.font = UIFont.HeitiSC(16)
        //titlelabel.isSkeletonable = true
        contentView.addSubview(titlelabel)
        
        subTitleLabel           = UILabel(frame: CGRect(x: 10, y: 35, width: 290, height: 10))
        subTitleLabel.font      = UIFont.AvenirLight(8)
        subTitleLabel.textColor = UIColor.gray
        //subTitleLabel.isSkeletonable = true
        contentView.addSubview(subTitleLabel)
        
        theimageView = UIImageView(frame: CGRect(x: 75, y: 0, width: 30, height: 30))
        theimageView.image = UIImage.init(named: "0135")
        //theimageView.isSkeletonable = true
        contentView.addSubview(theimageView)
        
        //self.isSkeletonable = true
         
    }
    
    override func loadContent() {
   
        let item           = data as! RedModel
        titlelabel.text    = item.name
        subTitleLabel.text = item.name2
         
        let label1heroID = "label1_\(String(describing: indexPath?.row))"
        let label2heroID = "label2_\(String(describing: indexPath?.row))"
        let imageheroID = "image_\(String(describing: indexPath?.row))"
        
//        titlelabel?.hero.id          = label1heroID
//        subTitleLabel?.hero.id       = label2heroID
//        theimageView?.hero.id        = imageheroID
    }
    
    override func selectedEvent() {
        
        delegate?.customCell(self, event: data)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        UIView.animate(withDuration: 0.35, delay: 0, options: .beginFromCurrentState, animations: {
            
            self.titlelabel.alpha = (highlighted == true ? 0.5    : 1.0)
            self.subTitleLabel.x  = (highlighted == true ? 10 + 4 : 10)
            
        }, completion: nil)
    }
}
