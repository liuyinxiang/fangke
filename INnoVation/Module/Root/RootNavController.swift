//
//  RootNavController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/22.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class RootNavController: BaseCustomNavigationController {
 
    override func makeConfig() {
        

        //This is the default animation for navigation controller provided by Hero. To disable the push animation, set self.hero.navigationAnimationType to .fade or .none on the navigation controller.
//        /hero.navigationAnimationType = .push(direction: .left)
    }

}
