//
//  RootTabBarController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/21.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit

class RootTabBarController: BaseCustomTabBarController, UITabBarControllerDelegate {

    override func creatSubViewControllers() {
        
  
        self.delegate = self
        
        //hero.tabBarAnimationType = .fade
        //hero.isEnabled  = false
        //tabBar.layer.removeAllAnimations()
        
        //半透明 默认true
        //tabBar.isTranslucent = false
        
        //iCarouselViewController
        //PageViewController
        //JSViewController
        //GakkiViewController
        //RxViewController
        //ServerViewController
        //ClientViewController
        //ShowViewController
        //AnimateViewController
        let VC1 = ServerViewController()
        addChildViewController(VC1,
                               title        : "Server",
                               image        : UIImage(named: "tab_mine"),
                               selectedImage: UIImage(named: "tab_mine_S"))
        
        let VC2 = ClientViewController()
        addChildViewController(VC2,
                               title        : "Client",
                               image        : UIImage(named: "tab_mine"),
                               selectedImage: UIImage(named: "tab_mine_S"))
        
        //
        //RedViewController
        let VC3 = RedViewController()
        addChildViewController(VC3,
                               title        : "Page3",
                               image        : UIImage(named: "tab_mine"),
                               selectedImage: UIImage(named: "tab_mine_S"))
        
        let VC4 = JSViewController()
        addChildViewController(VC4,
                               title        : "首页",
                               image        : UIImage(named: "tab_mine"),
                               selectedImage: UIImage(named: "tab_mine_S"))
 
    }

    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if #available(iOS 10.0, *) {
            let feedBackGenertor = UIImpactFeedbackGenerator(style: .light)
            feedBackGenertor.impactOccurred()
        }
        
        if viewController.isEqual(tabBarController.selectedViewController) {
            let nav = viewController        as! BaseCustomNavigationController
            let vc  = nav.topViewController as! BaseCustomViewController
            if  vc.isLoading == false{
                vc.shouldReload()
                print("开始下拉刷新")
            }
            
        }
        return true
    }
    
    
    
 

}
