//
//  BaseCustomNavigationController.swift
//  Swift-Animations
//
//  Created by YouXianMing on 2017/6/29.
//  Copyright © 2017年 YouXianMing. All rights reserved.
//

import UIKit
//import Hero

class BaseCustomNavigationController: UINavigationController {

    /// Init with rootViewController.
    ///
    /// - Parameters:
    ///   - rootViewController: An UIViewController used as rootViewController.
    ///   - hideTabBar: Navigation bar hide or not.
    convenience init(rootViewController: BaseCustomViewController, hideNavBar : Bool) {
        
        self.init(rootViewController : rootViewController)
        rootViewController.useInteractivePopGestureRecognizer()
        setNavigationBarHidden(hideNavBar, animated: false)
        
        makeConfig()
       
        
    }
    
    
    // empty
    func makeConfig(){}
    
    // MARK: -- System method && Debug message --
    
    deinit {
        
        print("[❌] '[NAV] " + String(describing: self.classForCoder) + "' is released.")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        print("[➡️] enter to --> '[NAV] " + String(describing: self.classForCoder) + "'.")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        print("[🕒] leave from <-- '[NAV] " + String(describing: self.classForCoder) + "'.")
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController?.hero.isEnabled = true
        
        guard let interactionGes = interactivePopGestureRecognizer else { return }
        guard let targetView = interactionGes.view else { return }
        guard let internalTargets = interactionGes.value(forKeyPath: "targets") as? [NSObject] else { return }
        guard let internalTarget = internalTargets.first?.value(forKey: "target") else { return }
        let action = Selector(("handleNavigationTransition:"))
        
        let fullScreenGesture = UIPanGestureRecognizer(target: internalTarget, action: action)
        fullScreenGesture.delegate = self
        targetView.addGestureRecognizer(fullScreenGesture)
        interactionGes.isEnabled = false
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if viewControllers.count > 0 {
            
            // 1. 自动隐藏tabbar
            viewController.hidesBottomBarWhenPushed = true
            
            // 2. Fix a Hero'BUG
            let frame = self.tabBarController?.tabBar.frame
            let height = UIScreen.main.bounds.height + 100
            UIView.animate(withDuration: 0.25, animations: {
                self.tabBarController?.tabBar.frame = CGRect(x: (frame?.origin.x)!, y: height, width: (frame?.width)!, height: (frame?.height)!)
            }) { (bool) in
            }
        }
 
        super.pushViewController(viewController, animated: animated)
    }
}

extension BaseCustomNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let isLeftToRight = UIApplication.shared.userInterfaceLayoutDirection == .leftToRight
        guard let ges = gestureRecognizer as? UIPanGestureRecognizer else { return true }
        if ges.translation(in: gestureRecognizer.view).x * (isLeftToRight ? 1 : -1) <= 0
            || value(forKey: "_isTransitioning") as! Bool
            || disablePopGesture {
            return false
        }
        return viewControllers.count != 1;
    }
}

extension BaseCustomNavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        guard let topVC = topViewController else { return .lightContent }
        return topVC.preferredStatusBarStyle
    }
}


enum UNavigationBarStyle {
    case theme
    case clear
    case white
}

extension BaseCustomNavigationController {
    
    private struct AssociatedKeys {
        static var disablePopGesture: Void?
    }
    
    var disablePopGesture: Bool {
        get {
            tabBarController?.tabBar.layer.removeAllAnimations()// A Hero's BUG Fix
            return objc_getAssociatedObject(self, &AssociatedKeys.disablePopGesture) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.disablePopGesture, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func barStyle(_ style: UNavigationBarStyle) {
        switch style {
        case .theme:
            navigationBar.barStyle = .black
            navigationBar.setBackgroundImage(UIImage(named: "nav_bg"), for: .default)
            navigationBar.shadowImage = UIImage()
        case .clear:
            navigationBar.barStyle = .black
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
        case .white:
            navigationBar.barStyle = .default
//            navigationBar.setBackgroundImage(UIColor.white.image(), for: .default)
//            navigationBar.shadowImage = nil
        }
        
        
    }
}




