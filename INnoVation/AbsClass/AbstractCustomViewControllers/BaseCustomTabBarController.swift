//
//  BaseCustomTabBarController.swift
//  INnoVation
//
//  Created by INnoVation on 2018/8/21.
//  Copyright © 2018 INnoVation. All rights reserved.
//

import UIKit
import AudioToolbox

class BaseCustomTabBarController: UITabBarController{

    override func viewDidLoad() {
        super.viewDidLoad()
         
        self.creatSubViewControllers()
        // Do any additional setup after loading the view.
    }
    
    func creatSubViewControllers(){
        
    }
    
    func addChildViewController(_ childController: BaseCustomViewController, title:String?, image:UIImage? ,selectedImage:UIImage?) {
        
        childController.title = title
        childController.tabBarItem = UITabBarItem(title: nil,
                                                  image: image?.withRenderingMode(.alwaysOriginal),
                                                  selectedImage: selectedImage?.withRenderingMode(.alwaysOriginal))
        
        childController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: CGFloat(12))], for: .normal)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            childController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
        
        addChild(BaseCustomNavigationController(rootViewController: childController, hideNavBar: true))
    }
    
  
   
 
}
