
//  KApi.swift
//  Created by KJoe on 2018/3/7.


import Moya
import SwiftyJSON
import PromiseKit

//初始化豆瓣FM请求的provider
let KJoeApiProvider = MoyaProvider<KJoeApi>()
 
/** 下面定义豆瓣FM请求的endpoints（供provider使用）**/

//请求分类
public enum KJoeApi {
    case channels  //获取频道列表
    case playlist(String) //获取歌曲
    case upload(img:UIImage) //上传文件
    case reying
    case banner(type:String)
    case scanTime(visit_id:String)
}

//http://18.191.208.172/visitor/web/index.php/front/visit/insert-scan-time
//请求配置
extension KJoeApi: TargetType {
    //服务器地址
    public var baseURL: URL {
//        switch self {
//        case .channels:
//            return URL(string: "https://www.douban.com")!
//        case .playlist(_):
//            return URL(string: "https://douban.fm")!
//        case .banner(_):
//            return URL(string: "http://api.wtlyhqb.com/onewallet-webapi")!
//        default:
//            return URL(string: "https://www.douban.com")!
//        }
//        return URL(string: "http://182.61.177.56/visitor/web/index.php/front/")!
//        return URL(string: "http://182.61.177.56/index.php/front/")!
        return URL(string: "http://182.61.177.56/front/")!


    }
    //各个请求的具体路径
    public var path: String {
        switch self {
        case .channels:
            return "/j/app/radio/channels"
        case .playlist(_):
            return "/j/mine/playlist"
        case .upload( _):
            return "/j/mine/playlist"
        case .reying:
            return "/v2/movie/in_theaters"
        case .banner:
            return "/wallet/getAdverts"
        case .scanTime:
            return "visit/insert-scan-time"
        }
    }
    
    //请求类型
    public var method: Moya.Method {
        return .post
    }
    
    //请求任务事件（这里附带上参数）
    public var task: Task {
        switch self {
        case .scanTime(let visit_id):
            var params: [String: Any] = [:]
            params["visit_id"] = visit_id
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
            
        case .playlist(let channel):
            var params: [String: Any] = [:]
            params["channel"] = channel
            params["type"] = "n"
            params["from"] = "mainsite"
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
        case .reying:
            var params: [String: Any] = [:]
            params["city"] = "北京"
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
            
        case let.banner(type):
            var params: [String: Any] = [:]
            params["advertType"] =  type
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.default)
            
        case let .upload(img):
            
            var params: [String: Any] = [:]
            params["type"] = "n"
            params["from"] = "mainsite"
            
            let data = img.jpegData(compressionQuality: 0.7)
            let img = MultipartFormData(provider: .data(data!), name: "参数名", fileName: "名称随便写.jpg", mimeType: "image/jpeg")
            return .uploadCompositeMultipart(([img]), urlParameters: params)
        default:
            return .requestPlain
        }
    }
    
    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }
    
    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
    
    //请求头
    public var headers: [String: String]? {
        return nil
    }
    
}



func CallApi(_ target: KJoeApi) -> Promise<String> {
    
    let provider = MoyaProvider<KJoeApi>()
    return Promise<String> { seal in
        provider.request(target, completion: { (result) in
            switch result {
            case let .success(response):
                do {
                    //如果数据返回成功则直接将结果转为JSON
                    let json = try JSON(response.mapJSON())
                    uLog("🚀🚀🚀🌟🌟🌟🌙🌙🌙response == \(json)")
                    
                    guard let jsonStr = json["channels"].rawString()else {
                        uLog("jsonStr为空")
                        return
                    }
                    seal.fulfill(jsonStr)
                } catch {
                    
                    seal.reject(error)
                    
                    let message = "CallApi: try JSON(response.mapJSON()) failed"
                    let alertViewController = UIAlertController(title: "请求失败",
                                                                message: "\(message)",
                        preferredStyle: .alert)
                    alertViewController.addAction(UIAlertAction(title: "确定", style: .default,
                                                                handler: nil))
                    topVC?.present(alertViewController, animated: true)
                    
                }
            case let .failure(error):
                
                let message = error.errorDescription ?? "未知错误"
                let alertViewController = UIAlertController(title: "请求失败",
                                                            message: "\(message)",
                    preferredStyle: .alert)
                alertViewController.addAction(UIAlertAction(title: "确定", style: .default,
                                                            handler: nil))
                topVC?.present(alertViewController, animated: true)
                
                seal.reject(error)
            }
        })
    }
}

